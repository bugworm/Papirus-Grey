You need to have Papirus-Dark icons installed: https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
To install this theme, put Papirus-Grey directory to your icons directory(/usr/share/icons or ~/.local/share/icons)
Icons and scripts was taken from Sardi Mono Papirus theme: https://github.com/erikdubois/Sardi